import 'package:get_it/get_it.dart';

import 'NavigatorService.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}
