import 'package:book_store/Resources/RouteConst.dart';
import 'package:book_store/Screens/DetailsPage/DetailsPage.dart';
import 'package:book_store/Screens/HomePage/HomePage.dart';
import 'package:book_store/Utils/WebViewUtil.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RouteConst.routeDefault:
        return MaterialPageRoute(builder: (_) => HomePage());
        break;

      case RouteConst.routeHomePage:
        return MaterialPageRoute(builder: (_) => HomePage());
        break;

      case RouteConst.routeDetailsPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
            builder: (_) => DetailsPage(
              id: data['data']['id'],
              name: data['data']['name'],
            ),
          );
        }
        break;

      case RouteConst.routeWebViewPage:
        {
          if (args != "" && args != null) {
            Map<String, dynamic> data;
            data = args;
            return MaterialPageRoute(
              builder: (_) => WebViewUtil(
                loadUrl: data['data']['webUrl'],
                pageType: 0,
                id: data['data']['id'],
              ),
            );
          }
        }
        break;

      default:
        return _errorRoute(settings.name);
    }
  }

  static Route<dynamic> _errorRoute(pageName) {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.error_outline),
              Text(
                "Error Routing Page Not Found : " + pageName.toString(),
                style: TextStyle(fontSize: 18.0),
              )
            ],
          ),
        ),
      );
    });
  }
}
