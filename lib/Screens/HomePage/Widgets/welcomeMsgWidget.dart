import 'package:flutter/material.dart';

// Display this widget only when there is no search key
Widget displayWelcomeWidget(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 30.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image(
          image: AssetImage('assets/images/book.png'),
          height: 60,
          width: 60,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "Welcome to IT Bookstore",
          style: Theme.of(context).textTheme.headline2,
        ),
        SizedBox(
          height: 10.0,
        ),
        Text(
          "Please search above with the Book Name you want to purchase",
          style: TextStyle(
              fontWeight: FontWeight.w600, color: Colors.black, fontSize: 15.0),
          textAlign: TextAlign.center,
        )
      ],
    ),
  );
}
