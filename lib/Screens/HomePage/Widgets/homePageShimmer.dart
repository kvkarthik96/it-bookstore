import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget albumListShimmer() {
  return Container(
    child: ListView.builder(
      itemCount: 15,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          child: Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
            child: Container(
              height: 180,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey[400],
            ),
          ),
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[400],
        );
      },
    ),
  );
}
