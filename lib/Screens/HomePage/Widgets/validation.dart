String validateSearch(String search) {
  if (search.length == 0) {
    return "Please enter your search key";
  } else if (search.length <= 1) {
    return "Search key lenth should be greater than 1 character";
  } else {
    return null;
  }
}
