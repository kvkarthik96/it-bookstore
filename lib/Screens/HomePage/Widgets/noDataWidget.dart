import 'package:flutter/material.dart';

// Display when no data found
Widget noDataWidget() {
  return Container(
    width: double.infinity,
    padding: const EdgeInsets.only(bottom: 30.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image(
          image: AssetImage('assets/images/noRecord.png'),
          height: 60,
          width: 60,
        ),
        SizedBox(
          height: 10.0,
        ),
        Text(
          "Search result not Found",
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w700),
        ),
      ],
    ),
  );
}
