import 'package:flutter/material.dart';

Widget booksCardWidget(resData) {
  return Card(
    child: Column(
      children: [
        new ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(2.0)),
          child: Container(
            height: 180,
            width: double.infinity,
            child: Image(
              image: NetworkImage(
                resData.image,
              ),
              fit: BoxFit.fitHeight,
              height: 200,
              width: double.infinity,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    resData.title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  resData.price,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      color: Colors.blue),
                ),
              ),
            ],
          ),
        ),
        if (resData.subtitle != "")
          Padding(
            padding:
                const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                resData.subtitle,
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    color: Colors.grey),
              ),
            ),
          ),
      ],
    ),
  );
}
