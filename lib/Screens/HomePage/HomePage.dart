import 'dart:ui';
import 'package:book_store/Resources/RouteConst.dart';
import 'package:book_store/Screens/HomePage/Widgets/booksCardWidget.dart';
import 'package:book_store/Screens/HomePage/Widgets/validation.dart';
import 'package:book_store/Utils/ErrorwidgetUtil.dart';
import 'package:book_store/Utils/HelperUtil.dart';
import 'package:book_store/Utils/NoInternetUtil.dart';
import 'package:book_store/Utils/ToastUtil.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'Widgets/appExitModel.dart';
import 'Widgets/homePageShimmer.dart';
import 'Widgets/noDataWidget.dart';
import 'Widgets/welcomeMsgWidget.dart';
import 'bloc/homepage_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final homeBloc = HomepageBloc();
  bool isNetworkConnected = true;
  var _connectivitySubscription;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final TextEditingController searchController = new TextEditingController();
  final FocusNode searchFocusNode = FocusNode();
  String searchKey = "";
  final key = GlobalKey<FormFieldState>();

  bool isSearchBarEnabled = false;

  @override
  void initState() {
    super.initState();

    onNetworkChange();

    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });
    // service call
    homeBloc.add(GetBooksEvent(
        page: 1, search: searchKey, books: [], showShimmer: true));
  }

  @override
  void dispose() {
    homeBloc.close();
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomepageBloc, HomepageState>(
        // cubit: homeBloc,
        buildWhen: (prevState, state) {
      if (state is HomepageInitialState) {
        return false;
      }
    }, builder: (context, state) {
      if (state is HomepageLoadingState) {
        return WillPopScope(
          onWillPop: () {
            return openExitModel(context: context);
          },
          child: Scaffold(
            appBar: appbar(),
            body: !isNetworkConnected
                ? NoInternetUtil(retryInternetCallBack: callApiService)
                : albumListShimmer(),
          ),
        );
      } else if (state is HomepageLoadedState) {
        return WillPopScope(
          onWillPop: () {
            return openExitModel(context: context);
          },
          child: Scaffold(
            appBar: appbar(),
            body: !isNetworkConnected
                ? NoInternetUtil(retryInternetCallBack: callApiService)
                : Column(
                    children: [
                      if (isSearchBarEnabled) searchBarWidget(),
                      Expanded(
                        child: (state.resData != null &&
                                state.resData.books.length > 0)
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  child: NotificationListener<
                                      ScrollEndNotification>(
                                    onNotification:
                                        (ScrollNotification scrollState) {
                                      FocusScope.of(context).unfocus();

                                      if (!state.saving &&
                                          scrollState.metrics.pixels ==
                                              scrollState
                                                  .metrics.maxScrollExtent) {
                                        if (state.resData != null &&
                                            state.resData.total != "0") {
                                          state.saving = true;

                                          homeBloc.add(GetBooksEvent(
                                              page: int.parse(
                                                      state.resData.page) +
                                                  1,
                                              search: searchKey,
                                              books: state.resData.books,
                                              resData: state.resData));
                                        }
                                      }
                                    },
                                    child: SmartRefresher(
                                      controller: _refreshController,
                                      enablePullDown: false,
                                      enablePullUp: (state.resData != null &&
                                              state.resData.total != "0" &&
                                              isNetworkConnected == true)
                                          ? true
                                          : false,
                                      child: ListView.builder(
                                          itemCount: state.resData.books.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Column(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    HelperUtil
                                                            .checkInternetConnection()
                                                        .then((internet) {
                                                      if (internet) {
                                                        Map<String, dynamic>
                                                            data = {
                                                          "id": state
                                                              .resData
                                                              .books[index]
                                                              .isbn13,
                                                          "name": state
                                                              .resData
                                                              .books[index]
                                                              .title
                                                        };

                                                        Navigator.of(context)
                                                            .pushNamed(
                                                                RouteConst
                                                                    .routeDetailsPage,
                                                                arguments: {
                                                              "data": data
                                                            });
                                                      } else {
                                                        ToastUtil().showMsg(
                                                            "No Internet connection!!!",
                                                            Colors.black,
                                                            Colors.white,
                                                            12.0,
                                                            "short",
                                                            "centre");
                                                      }
                                                    });
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            5.0),
                                                    child: booksCardWidget(state
                                                        .resData.books[index]),
                                                  ),
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                              )
                            : (searchKey == null || searchKey == "")
                                ? displayWelcomeWidget(context)
                                : noDataWidget(),
                      ),
                    ],
                  ),
          ),
        );
      } else {
        return WillPopScope(
          onWillPop: () {
            return openExitModel(context: context);
          },
          child: Scaffold(
            appBar: appbar(),
            body: ErrorWidgetClass(
              retryInternetCallBack: callApiService,
            ),
          ),
        );
      }
    });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        homeBloc.add(GetBooksEvent(
          page: 1,
          search: searchKey,
          books: [],
          showShimmer: true,
        ));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }

  void onTextChanged(String text) {
    searchKey = text;
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        homeBloc.add(
            GetBooksEvent(page: 1, search: text, books: [], showShimmer: true));
      }
    });
  }

  // App bar widget
  Widget appbar() {
    return AppBar(
      centerTitle: true,
      title: Text(
        "IT Bookstore",
        style: Theme.of(context).textTheme.headline1,
      ),
      actions: [
        IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 25,
            ),
            onPressed: () {
              setState(() {
                isSearchBarEnabled = !isSearchBarEnabled;
                if (isSearchBarEnabled) {
                  searchFocusNode.requestFocus();
                } else {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              });
            })
      ],
    );
  }

  Widget searchBarWidget() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
      child: Container(
        // height: 60,
        child: Form(
          child: Container(
            decoration: new BoxDecoration(
              color: const Color(0xffffffff),
            ),
            child: TextFormField(
              key: key,
              validator: validateSearch,
              focusNode: searchFocusNode,
              controller: searchController,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.search,
              maxLength: 50,
              maxLengthEnforced: true,
              onFieldSubmitted: (text) {
                FocusScope.of(context).unfocus();
                if (key.currentState.validate()) {
                  onTextChanged(text);
                }
              },
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
              inputFormatters: [
                FilteringTextInputFormatter.deny(RegExp(r"\s\s")),
                FilteringTextInputFormatter.deny(RegExp(
                    r'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])')),
              ],
              decoration: InputDecoration(
                  hintText: "Search Books",
                  focusColor: Colors.green,
                  counterText: "",
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 1.0),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(9.0),
                  ),
                  hintStyle: TextStyle(
                      fontWeight: FontWeight.w500, color: Colors.grey),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    color: Colors.purple,
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      if (key.currentState.validate()) {
                        onTextChanged(searchController.text);
                      }
                    },
                  ),
                  errorMaxLines: 2,
                  errorStyle: TextStyle(
                      color: Colors.purple,
                      fontSize: 14,
                      fontStyle: FontStyle.normal)),
            ),
          ),
        ),
      ),
    );
  }
}
