part of 'homepage_bloc.dart';

@immutable
abstract class HomepageState {}

class HomepageInitialState extends HomepageState {}

class HomepageLoadingState extends HomepageState {}

class HomepageLoadedState extends HomepageState {
  final BookListModel resData;
  bool saving;

  HomepageLoadedState({this.resData, this.saving});
}

class HomepageErrorState extends HomepageState {}
