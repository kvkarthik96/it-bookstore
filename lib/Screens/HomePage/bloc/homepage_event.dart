part of 'homepage_bloc.dart';

@immutable
abstract class HomepageEvent {}

class GetBooksEvent extends HomepageEvent {
  final int page;
  final String search;
  final bool saving;
  List<Books> books = [];
  final showShimmer;
  BookListModel resData;
  GetBooksEvent(
      {this.search,
      this.page,
      this.saving,
      this.books,
      this.showShimmer = false,
      this.resData});
}
