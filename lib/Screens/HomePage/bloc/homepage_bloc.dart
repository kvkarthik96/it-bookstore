import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:book_store/DataModels/BookListModel.dart';
import 'package:book_store/Network/Service.dart';
import 'package:book_store/Utils/HelperUtil.dart';
import 'package:meta/meta.dart';

part 'homepage_event.dart';
part 'homepage_state.dart';

class HomepageBloc extends Bloc<HomepageEvent, HomepageState> {
  HomepageBloc() : super(HomepageInitialState());

  @override
  Stream<HomepageState> mapEventToState(
    HomepageEvent event,
  ) async* {
    if (event is GetBooksEvent) {
      yield* _mapGetBooksEventDatatoState(event);
    }
  }

  Stream<HomepageState> _mapGetBooksEventDatatoState(
      GetBooksEvent event) async* {
    BookListModel resData;
    List<Books> prevData = event.books;
    bool saving = true;

    try {
      getAlbumDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service()
                .getBooksList(searchKey: event.search, page: event.page)
                .then((respObj) {
              resData = respObj;
              if (resData.books != null && resData.books.length > 0) {
                if (prevData != null && prevData.length > 0) {
                  prevData.addAll(resData.books);
                  resData.books = event.books;
                }
              } else {
                resData.books = event.books;
              }
              saving = false;
            });
          } else {
            if (event.resData != null) {
              resData = event.resData;
            }
          }
        });
      }

      if (event.page == 1 && event.showShimmer == true) {
        yield HomepageLoadingState();
      } else {
        yield HomepageInitialState();
      }
      await getAlbumDetails();
      yield HomepageLoadedState(resData: resData, saving: saving);
    } catch (e) {
      yield HomepageErrorState();
    }
  }
}
