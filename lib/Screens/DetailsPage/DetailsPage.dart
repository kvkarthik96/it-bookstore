import 'package:book_store/Screens/DetailsPage/Widgets/Appbar.dart';
import 'package:book_store/Screens/DetailsPage/Widgets/BookDetailsWidget.dart';
import 'package:book_store/Utils/ErrorwidgetUtil.dart';
import 'package:book_store/Utils/HelperUtil.dart';
import 'package:book_store/Utils/NoInternetUtil.dart';
import 'package:book_store/Utils/ToastUtil.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'Widgets/DetailsPageShimmer.dart';
import 'bloc/detailspage_bloc.dart';

class DetailsPage extends StatefulWidget {
  final String id;
  final String name;

  const DetailsPage({Key key, this.id = "0", this.name = ""}) : super(key: key);
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  final bloc = DetailspageBloc();
  bool isNetworkConnected = true;
  var _connectivitySubscription;

  @override
  void initState() {
    super.initState();

    onNetworkChange();

    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });
    bloc.add(GetBookDetailsEvent(id: widget.id));
  }

  @override
  void dispose() {
    bloc.close();
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailspageBloc, DetailspageState>(
        // cubit: bloc,
        builder: (context, state) {
      if (state is DetailspageLoadingState) {
        return WillPopScope(
          onWillPop: () {
            Navigator.pop(context);
          },
          child: Scaffold(
            appBar: detailsPageAppBar(widget.name, context),
            body: !isNetworkConnected
                ? NoInternetUtil(retryInternetCallBack: callApiService)
                : detailsPageShimmer(),
          ),
        );
      } else if (state is DetailspageLoadedState) {
        return WillPopScope(
          onWillPop: () {
            Navigator.pop(context);
          },
          child: Scaffold(
              appBar: detailsPageAppBar(widget.name, context),
              body: !isNetworkConnected
                  ? NoInternetUtil(retryInternetCallBack: callApiService)
                  : bookDetailsWidget(state.resData, context, widget.id)),
        );
      } else {
        return WillPopScope(
          onWillPop: () {
            Navigator.pop(context);
          },
          child: Scaffold(
            appBar: detailsPageAppBar(widget.name, context),
            body: ErrorWidgetClass(
              retryInternetCallBack: callApiService,
            ),
          ),
        );
      }
    });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        bloc.add(GetBookDetailsEvent(id: widget.id));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }
}
