import 'package:flutter/material.dart';

import 'descWidget.dart';

Widget bookDetailsWidget(resData, BuildContext context, String id) {
  return Container(
    width: double.infinity,
    child: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 200,
              child: Image(
                image: NetworkImage(resData.image),
                fit: BoxFit.fitWidth,
              ),
            ),
            Container(
              child: Text(
                resData.title,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 6,
            ),
            if (resData.subtitle != null && resData.subtitle != "")
              Container(
                child: Text(
                  "( ${resData.subtitle} )",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            SizedBox(
              height: 10,
            ),
            descWidget("Year", resData.year, context),
            descWidget("Authors", resData.authors, context),
            descWidget("Publisher", resData.publisher, context),
            descWidget("Language", resData.language, context),
            descWidget("Rating", resData.rating, context),
            descWidget("Price", resData.price, context),
            descWidget("Description", resData.desc, context),
            descWidget("Link", resData.url, context, id: id),
          ],
        ),
      ),
    ),
  );
}
