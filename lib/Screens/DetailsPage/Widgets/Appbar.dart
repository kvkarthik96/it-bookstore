import 'package:flutter/material.dart';

Widget detailsPageAppBar(String title, BuildContext context) {
  return AppBar(
    elevation: 0,
    centerTitle: true,
    title: FittedBox(
      child: Text("Book Details", style: Theme.of(context).textTheme.headline1),
    ),
  );
}
