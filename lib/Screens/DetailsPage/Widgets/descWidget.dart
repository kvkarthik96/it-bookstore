import 'package:book_store/Resources/RouteConst.dart';
import 'package:flutter/material.dart';

Widget descWidget(String name, String value, BuildContext context,
    {String id}) {
  return Padding(
    padding: const EdgeInsets.only(top: 10.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 3,
          child: Container(
            child: Text(
              name,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 15.0,
              ),
            ),
          ),
        ),
        Text(
          ' : ',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        Expanded(
          flex: 7,
          child: (name != "Link")
              ? Container(
                  child: Text(
                    "$value",
                    style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                      fontSize: 15.0,
                    ),
                  ),
                )
              : InkWell(
                  child: Text(
                    value,
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  onTap: () {
                    Map<String, dynamic> data = {};
                    data['data'] = {
                      "webUrl": value,
                      "headText": name,
                      "id": id
                    };
                    Navigator.of(context).popAndPushNamed(
                      RouteConst.routeWebViewPage,
                      arguments: data,
                    );
                  },
                ),
        )
      ],
    ),
  );
}
