part of 'detailspage_bloc.dart';

@immutable
abstract class DetailspageEvent {}

class GetBookDetailsEvent extends DetailspageEvent {
  final String id;

  GetBookDetailsEvent({this.id});
}
