import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:book_store/DataModels/BookDetailsModel.dart';
import 'package:book_store/Network/Service.dart';
import 'package:book_store/Utils/HelperUtil.dart';
import 'package:meta/meta.dart';

part 'detailspage_event.dart';
part 'detailspage_state.dart';

class DetailspageBloc extends Bloc<DetailspageEvent, DetailspageState> {
  DetailspageBloc() : super(DetailspageLoadingState());

  @override
  Stream<DetailspageState> mapEventToState(
    DetailspageEvent event,
  ) async* {
    if (event is GetBookDetailsEvent) {
      yield* _maptoGetBookDetailseventDatatoState(event);
    }
  }

  Stream<DetailspageState> _maptoGetBookDetailseventDatatoState(
      GetBookDetailsEvent event) async* {
    BookDetailsModel resData;

    try {
      getAlbumDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service().getPhotosDetails(id: event.id).then((respObj) {
              resData = respObj;
            });
          }
        });
      }

      yield DetailspageLoadingState();
      await getAlbumDetails();
      yield DetailspageLoadedState(resData: resData);
    } catch (e) {
      yield DetailspageErrorState();
    }
  }
}
