part of 'detailspage_bloc.dart';

@immutable
abstract class DetailspageState {}

class DetailspageInitialState extends DetailspageState {}

class DetailspageLoadingState extends DetailspageState {}

class DetailspageLoadedState extends DetailspageState {
  final BookDetailsModel resData;

  DetailspageLoadedState({this.resData});
}

class DetailspageErrorState extends DetailspageState {}
