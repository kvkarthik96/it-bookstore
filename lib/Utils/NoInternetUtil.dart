import 'package:flutter/material.dart';

class NoInternetUtil extends StatelessWidget {
  final Function retryInternetCallBack;

  const NoInternetUtil({Key key, this.retryInternetCallBack}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "No internet connection!!!",
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Please try again later",
            style: TextStyle(color: Colors.black, fontSize: 14.0),
          ),
          SizedBox(
            height: 30,
          ),
          RaisedButton(
              child: Text('Retry'),
              onPressed: () {
                retryInternetCallBack();
              })
        ],
      ),
    );
    ;
  }
}
