import 'dart:async';
import 'package:book_store/Resources/RouteConst.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'HelperUtil.dart';
import 'LoadingUtil.dart';

final Set<JavascriptChannel> jsChannels = [
  JavascriptChannel(
      name: 'Print', onMessageReceived: (JavascriptMessage message) {}),
].toSet();

class WebViewUtil extends StatefulWidget {
  final int pageType;
  final String loadUrl;
  final String id;

  const WebViewUtil({Key key, this.pageType = 0, this.loadUrl, this.id})
      : super(key: key);

  @override
  _WebViewUtilState createState() => _WebViewUtilState();
}

class _WebViewUtilState extends State<WebViewUtil> with WidgetsBindingObserver {
  /* Instance of WebView plugin */
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  /* Webview Streams */
  StreamSubscription _onDestroy;
  StreamSubscription<String> _onUrlChanged;
  StreamSubscription<WebViewStateChanged> _onStateChanged;
  StreamSubscription<WebViewHttpError> _onHttpError;

  /* Sync Lang Text */
  String internetAlert = '';
  String noInternetMsg = "Oops! No Internet Connection";
  String noInteenetDesc =
      "Make sure wifi or cellular data is turned on and then try again.";
  String retryBtn = "Try Again";

  /* Internet Listeners */
  var connectivitySubscription;
  bool isNetworkConnected = false;
  var currentUrl = '';
  ConnectivityResult previousConRes;
  var loadingStyle = '''
    <style>
        body {
          position: relative;
          display:table-cell;
          width:100vh;
          height:100vh;
          text-align:center;
          vertical-align:middle;
        }
        .loader {
          margin: 0;
          display: inline-block;
          top: 50%;
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          border: 10px solid #f3f3f3;
          border-radius: 50%;
          border-top: 10px solid #4378FF;
          width: 50px;
          height: 50px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }      /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }      @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
        </style>
  ''';

  /* On change in network wifi/mobile data */
  void onNetworkChange() {
    connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      ConnectivityResult currentConRes = result;
      if (currentConRes != previousConRes) {
        previousConRes = currentConRes;
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
          flutterWebViewPlugin.reloadUrl(
              Uri.dataFromString(internetHTMLData(), mimeType: 'text/html')
                  .toString());
        } else if (result == ConnectivityResult.mobile) {
          isNetworkConnected = true;
        } else if (result == ConnectivityResult.wifi) {
          isNetworkConnected = true;
        }
      }
      setState(() {});
    });
  }

  String internetHTMLData() {
    String retryURL = widget.loadUrl + "/native?type=24&id=";
    return '''
      <!doctype html>
      <html>
      <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>

      <title>Please check your internet connection</title>
      <style>
        @font-face {
        font-family: 'product_sansregular';
        src: url('./fonts/productsans-regular-webfont.woff2') format('woff2'),
          url('./fonts/productsans-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
      }
      :root {
        font-size: 16px;
      }
      body {
        font-family: 'product_sansregular';
      }
      h1 {
        font-size: 1.25rem;
      }
        p {
          font-size: 0.875rem;
          line-height: 1.25rem;
        }
      .main-title {
        line-height: 1.65rem;
        color: #121036;
        margin: 0px;
      }
        .no-internet {
        position: fixed;
        width: 100%;
        height: 100vh;
        background: #ffffff;
        padding: 0px 30px;
        box-sizing: border-box;
        overflow: hidden;
        z-index: 1;
      }
      .no-internet .no-internet-wrapper {
        padding: 50px 0px 0px 0px;
      }
      .no-internet-wrapper .img {
        display: inline-block;
        width: 65px;
        margin: 0px 0px 20px 0px;
      }
      .no-internet-wrapper .img svg {
        width:100%;
        float: left;
      }
      .txt-a-c {
        text-align: center;
      }
      .bx,.space1,.space2,.space3,.space4,.space5 {
        position: relative;
        width: 100%;
        float: left;
      }
      .space1 {
        height: 10px;
      }
      .space2 {
        height: 20px;
      }
      .cta {
        border: none;
        line-height: 2.5rem;
        padding: 0px 20px;
        border-radius: 25px;
      }
      .primary-cta {
        color: #ffffff;
        background: #4378FF;
      }
      </style>
      <script type="text/javascript">
          window.history.forward();
          function noBack() {
              window.history.forward();
          }
      </script>
      </head>
      <body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="" class="no-internet">
      <section class="no-internet-wrapper txt-a-c">
        <div class="img">
          <svg version="1.1" x="0px" y="0px"
        viewBox="0 0 111 272" style="enable-background:new 0 0 111 272;" xml:space="preserve">
      <style type="text/css">
        .st0{fill:#E7E0F2;}.st1{fill:url(#SVGID_1_);}.st2{opacity:0.3;fill:#030C37;}.st3{fill:#FFFFFF;}	.st4{opacity:0.5;fill:none;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}.st5{fill:#030C37;}.st6{opacity:0.8;fill:#121036;}	.st7{fill:#FFFFFF;stroke:#121036;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}</style><g><path class="st0" d="M111.3,258.4c0,7.7-6.2,13.9-13.9,13.9H14c-7.7,0-13.9-6.2-13.9-13.9V84.1C0,76.4,6.3,70.2,14,70.2h83.5
          c7.7,0,13.9,6.2,13.9,13.9V258.4z"/></g><g><linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="24.5962" y1="99.3849" x2="83.2629" y2="214.0515"><stop  offset="0" style="stop-color:#2B1661"/><stop  offset="1" style="stop-color:#3A0092"/></linearGradient><path class="st1" d="M103.4,238.5c0,2-1.7,3.7-3.7,3.7H11.6c-2,0-3.7-1.7-3.7-3.7V81.8c0-2,1.7-3.7,3.7-3.7h88.1
          c2,0,3.7,1.7,3.7,3.7V238.5z"/></g><path class="st2" d="M65,256.7c0,5.1-4.2,9.3-9.3,9.3c-5.1,0-9.3-4.2-9.3-9.3c0-5.1,4.2-9.3,9.3-9.3C60.8,247.5,65,251.6,65,256.7z"
        /><g><path class="st3" d="M39.5,134.6c-3.2,0-5.9,2.6-5.9,5.9c0,3.2,2.6,5.9,5.9,5.9c3.2,0,5.9-2.6,5.9-5.9
          C45.3,137.3,42.7,134.6,39.5,134.6z"/><path class="st3" d="M71.9,134.6c-3.2,0-5.9,2.6-5.9,5.9c0,3.2,2.6,5.9,5.9,5.9c3.2,0,5.9-2.6,5.9-5.9
          C77.8,137.3,75.2,134.6,71.9,134.6z"/></g><path class="st4" d="M35,181.6c0,0,16.7-25.3,41.4-10.8"/>
      <path class="st5" d="M44.2,140.6c0,1.1-0.9,1.9-1.9,1.9c-1.1,0-1.9-0.9-1.9-1.9s0.9-1.9,1.9-1.9C43.4,138.7,44.2,139.5,44.2,140.6z"
        /><path class="st5" d="M76.7,140.6c0,1.1-0.9,1.9-1.9,1.9c-1.1,0-1.9-0.9-1.9-1.9s0.9-1.9,1.9-1.9C75.8,138.7,76.7,139.5,76.7,140.6z"
        /><path class="st6" d="M38.2,47.5l2.5,3.4c1.8-0.7,3.7-1.2,5.7-1.6l-1.7-3.8C42.4,46,40.3,46.6,38.2,47.5z"/>
      <path class="st6" d="M70.7,49c-5.2-2.7-11.1-4.3-17.3-4.4l0.7,4.1c5.1,0.2,9.8,1.5,14.1,3.6L70.7,49z"/><path class="st6" d="M30.5,37.1l2.5,3.3c2.5-1.2,5.2-2.2,8-3l-1.7-3.7C36.2,34.5,33.3,35.7,30.5,37.1z"/><path class="st6" d="M75.9,42.1l2.5-3.3c-7.5-4.5-16.2-7.1-25.5-7.1c-0.5,0-1,0-1.6,0l0.7,4c0.3,0,0.6,0,0.9,0
        C61.3,35.8,69.1,38.1,75.9,42.1z"/><path class="st6" d="M23,26.8l2.4,3.3c3.2-1.8,6.6-3.3,10.2-4.4L33.9,22C30.1,23.2,26.4,24.9,23,26.8z"/><path class="st6" d="M83.4,32.1l2.5-3.3c-9.5-6.2-20.8-9.9-33.1-9.9c-1.2,0-2.5,0-3.7,0.1l0.7,4c1-0.1,2-0.1,3-0.1
        C64.1,23,74.6,26.3,83.4,32.1z"/><path class="st6" d="M16.1,17l1.4,2.3l0.8,1.1c3.8-2.3,7.9-4.3,12.2-5.8l-1.7-3.7C24.4,12.5,20.1,14.6,16.1,17z"/>
      <path class="st6" d="M90.6,22.6l2.5-3.3C81.6,11.4,67.8,6.7,52.9,6.7c-1.9,0-3.8,0.1-5.7,0.2l0.7,4c1.7-0.1,3.3-0.2,5-0.2
        C66.9,10.8,79.8,15.2,90.6,22.6z"/><line class="st7" x1="25" y1="2" x2="55.9" y2="63.5"/></svg>
        </div>
        <h1 class="main-title">Oops! No<br>Internet Connection</h1>
        <div class="space1">&nbsp;</div>
        <p>Make sure wifi or cellular data<br>is turned on and then try again.</p>
        <div class="space2">&nbsp;</div>
        <div class="bx txt-a-c">
          <a href="$retryURL"><button class="btn cta primary-cta">Retry</button></a>
        </div>
      </section>
      </body>
      </html>
    ''';
  }

  String loadHTMLData(webUrl) {
    return '''
      <html>
        $loadingStyle
        <body onload="document.f.submit();">
          <div class="loader"></div>
          <form id="f" name="f" method="post" action="$webUrl">
          </form>
        </body>
      </html>
     ''';
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    onNetworkChange();
    setUpWebViewStreams();
  }

  setUpWebViewStreams() {
    flutterWebViewPlugin.close();
    _onDestroy = flutterWebViewPlugin.onDestroy.listen((_) {
      if (mounted) {}
    });

    _onUrlChanged = flutterWebViewPlugin.onUrlChanged.listen((String url) {
      if (mounted) {
        setState(() {
          currentUrl = url;
        });
      }
    });

    _onHttpError =
        flutterWebViewPlugin.onHttpError.listen((WebViewHttpError error) async {
      if (int.parse(error.code) < 0) {
        bool isConnected = await HelperUtil.checkInternetConnection();
        if (!isConnected) {
          flutterWebViewPlugin.reloadUrl(
              Uri.dataFromString(internetHTMLData(), mimeType: 'text/html')
                  .toString());
        }
      }
    });
  }

  @override
  void dispose() {
    _onDestroy?.cancel();
    _onUrlChanged?.cancel();
    _onStateChanged?.cancel();
    _onHttpError?.cancel();
    connectivitySubscription?.cancel();
    flutterWebViewPlugin?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: WillPopScope(
        onWillPop: () {
          handleBack();
        },
        child: WebviewScaffold(
          url: Uri.dataFromString(loadHTMLData(widget.loadUrl),
                  mimeType: 'text/html')
              .toString(),
          clearCache: true,
          resizeToAvoidBottomInset: true,
          javascriptChannels: jsChannels,
          mediaPlaybackRequiresUserGesture: false,
          withZoom: false,
          withLocalStorage: true,
          hidden: true,
          withJavascript: true,
          initialChild: Container(
            child: Center(child: LoadingUtil.ballRotate(context)),
          ),
          appBar: AppBar(
            elevation: 0.0,
            title: Text(
              "IT Book.store",
              style: Theme.of(context).textTheme.headline1,
            ),
            centerTitle: true,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 25,
                color: Colors.white,
              ),
              onPressed: () {
                handleBack();
              },
            ),
          ),
        ),
      ),
    );
  }

  void handleBack() {
    Map<String, dynamic> data = {"id": widget.id, "name": ""};

    Navigator.of(context).pushReplacementNamed(RouteConst.routeDetailsPage,
        arguments: {"data": data});
  }
}
