class RouteConst {
  static const String routeDefault = "/";
  static const routeHomePage = "/HomePage";
  static const routeDetailsPage = "/DetailsPage";
  static const routeWebViewPage = "/WebViewPage";
}
