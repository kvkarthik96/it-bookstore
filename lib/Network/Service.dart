import 'dart:async';
import 'dart:core';

import 'package:book_store/DataModels/BookDetailsModel.dart';
import 'package:book_store/DataModels/BookListModel.dart';

import 'Network.dart';

class Service {
  Network _network = new Network();

  Map<String, String> data = new Map<String, String>();

  Future<BookListModel> getBooksList({String searchKey, int page = 1}) {
    data['Content-Type'] = 'application/json';
    return _network
        .get('https://api.itbook.store/1.0/search/$searchKey/$page',
            headers: data)
        .then((dynamic res) {
      return new BookListModel.fromJson(res);
    });
  }

  Future<BookDetailsModel> getPhotosDetails({String id}) {
    data['Content-Type'] = 'application/json';
    return _network
        .get('https://api.itbook.store/1.0/books/$id', headers: data)
        .then((dynamic res) {
      return new BookDetailsModel.fromJson(res);
    });
  }
}
