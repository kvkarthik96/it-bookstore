import 'dart:convert';
import 'package:http/http.dart' as http;

class Network {
  /* Singleton Class */
  static Network _instance = new Network.internal();
  Network.internal();
  factory Network() => _instance;

  /* Json Decoder */
  final JsonDecoder _decoder = new JsonDecoder();

  /* Get Http Call */
  Future<dynamic> get(String url, {Map headers}) {
    return http.get(url, headers: headers).then((http.Response response) {
      final Map<dynamic, dynamic> res = _decoder.convert(response.body);
      return res;
    });
  }

  /* Post Http Call */
  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final Map<dynamic, dynamic> res = _decoder.convert(response.body);
      return res;
    });
  }
}
