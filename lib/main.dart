import 'dart:convert';

import 'package:book_store/Resources/RouteConst.dart';
import 'package:book_store/Routers/Locator.dart';
import 'package:book_store/Routers/NavigatorService.dart';
import 'package:book_store/Routers/Routing.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        textTheme: TextTheme(
          headline1: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
          headline2: TextStyle(
            color: Colors.purple,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: RouteConst.routeDefault,
      onGenerateRoute: RouteGenerator.generateRoute,
      navigatorKey: locator<NavigationService>().navigatorKey,
      debugShowCheckedModeBanner: false,
    );
  }
}
