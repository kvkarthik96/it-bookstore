import 'package:book_store/Screens/HomePage/Widgets/validation.dart';
import 'package:test/test.dart';

void main() {
  test('empty search returns error string', () {
    final result = validateSearch('');
    expect(result, 'Please enter your search key');
  });

  test('search length is equal to 1 character return error string', () {
    final result = validateSearch('a');
    expect(result, 'Search key lenth should be greater than 1 character');
  });

  test('search length greaterthan 1 character return  null', () {
    final result = validateSearch('java');
    expect(result, null);
  });
}
